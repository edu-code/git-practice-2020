FROM python:3.9

ARG BUILD_VERSION

LABEL name="Car-rest" \
      version=$BUILD_VERSION

RUN mkdir -p /data/flask/app
WORKDIR /data/flask/app
COPY requirements.txt /data/flask/app
RUN pip install --no-cache-dir -r requirements.txt

COPY . /data/flask/app

RUN chmod +x docker-entrypoint.sh

CMD ["/bin/bash", "docker-entrypoint.sh"]
