from flask import Flask


def create_app():
    app = Flask(__name__)

    @app.route('/status')
    def healthcheck():
        return 'OK'

    @app.route('/red00')
    def red00():
        return '00'

    return app


app = create_app()
